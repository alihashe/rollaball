﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{ //Variables set at the start
    public float speed;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    private int count;

    void Start()
    //Variables that go into effect as soon as the game starts
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText(); //Starts the counter for points
        winText.text = "";
    }

    void FixedUpdate()
    //Allows the player to move
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);

    }

    private void OnTriggerEnter(Collider other)
    //Allows the player to pick up items
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
    }

        
    void SetCountText()
    //Adds one to the win counter every time an item is collected
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 9)
        {
            winText.text = "You Win";

            Invoke("RestartLevel", 3f);
            /*If all items are collected, you win text appears and
            after 3 seconds the restart level function goes into
            effect.*/
        }
    }

    //EVERYTHING BELOW WAS ADDED AS A MODIFICATION
    void RestartLevel() //Reloads the scene when the player wins
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void Update() //This is a speed boost that is activated by the spacebar
    {
        bool onGround = Physics.Raycast(transform.position, Vector3.down, .52f);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            speed = speed * 2f;
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            speed = speed / 2f;
        }
    }

}
