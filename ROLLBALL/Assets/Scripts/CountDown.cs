﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountDown : MonoBehaviour
{
    public int timeLeft = 60;
    public CountDown Countdown;
    private string text;

    void Start()
    {
        StartCoroutine("LoseTime");
        Time.timeScale = 1;
    }

    void Update()
    {
        Countdown.text = ("" + timeLeft);
    }
    IEnumerator LoseTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            timeLeft--;
        }
    }
}
